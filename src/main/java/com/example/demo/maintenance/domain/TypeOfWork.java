package com.example.demo.maintenance.domain;

public enum TypeOfWork {
  PREVENTIVE, CORRECTIVE, OPERATIVE
}
