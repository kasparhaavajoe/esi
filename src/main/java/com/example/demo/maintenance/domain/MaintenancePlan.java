package com.example.demo.maintenance.domain;

import java.util.ArrayList;
import java.util.List;

import lombok.AccessLevel;
import lombok.Data;
import javax.persistence.*;
import com.example.demo.inventory.domain.PlantInventoryItem;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class MaintenancePlan {

  @Id @GeneratedValue
  Long id;

  @OneToMany(cascade={CascadeType.ALL})
  List<MaintenanceTask> tasks;

  @OneToOne
  PlantInventoryItem plant;

  public static MaintenancePlan of(PlantInventoryItem plant) {
    MaintenancePlan mp = new MaintenancePlan();
    mp.plant = plant;
    mp.tasks = new ArrayList<>();
    return mp;
  }
}
