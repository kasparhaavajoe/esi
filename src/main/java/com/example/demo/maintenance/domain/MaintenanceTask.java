package com.example.demo.maintenance.domain;

import java.math.BigDecimal;

import lombok.*;

import javax.persistence.*;
import com.example.demo.inventory.domain.PlantReservation;
import com.example.demo.common.domain.BusinessPeriod;

@Entity
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class MaintenanceTask {

  @Id @GeneratedValue
  Long id;

  String description;
  @Enumerated(EnumType.STRING)
  TypeOfWork type_of_work;
  @Column(precision=8, scale=2)
  BigDecimal price;

  @Embedded
  BusinessPeriod period;
}
