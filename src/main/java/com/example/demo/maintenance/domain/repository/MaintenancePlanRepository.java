package com.example.demo.maintenance.domain.repository;

import com.example.demo.maintenance.domain.MaintenancePlan;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MaintenancePlanRepository extends JpaRepository<MaintenancePlan, Long> {

}