package com.example.demo.common.domain;

import java.time.LocalDate;
import lombok.*;
import javax.persistence.*;

@Embeddable
@Value // immutability aka only getters
@NoArgsConstructor(force=true, access=AccessLevel.PRIVATE)
@AllArgsConstructor(staticName="of")
public class BusinessPeriod {
  LocalDate startDate;
  LocalDate endDate;
}
