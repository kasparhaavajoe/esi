package com.example.demo.inventory.domain.repository;

import com.example.demo.inventory.domain.PlantInventoryEntry;
import com.example.demo.inventory.domain.PlantInventoryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PlantInventoryItemRepository extends JpaRepository<PlantInventoryItem, Long> {

    PlantInventoryItem findOneByPlantInfo(PlantInventoryEntry entry);

}
