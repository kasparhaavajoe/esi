package com.example.demo.inventory.domain;

import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.maintenance.domain.MaintenancePlan;
import com.example.demo.common.domain.BusinessPeriod;
import javax.persistence.*;

import lombok.*;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class PlantReservation {

  @Id @GeneratedValue
  Long id;

  @OneToOne
  PlantInventoryItem plant;

  @OneToOne
  MaintenancePlan maint_plan;

  @ManyToOne
  PurchaseOrder rental;

  @Embedded
  BusinessPeriod period;

  public static PlantReservation of(PlantInventoryItem plant, BusinessPeriod period) {
    PlantReservation reservation = new PlantReservation();
    reservation.plant = plant;
    reservation.period = period;
    return reservation;
  }
}
