package com.example.demo.inventory.domain;

public enum EquipmentCondition {
  SERVICEABLE,
  UNSERVICEABLE_REPAIRABLE,
  UNSERVICEABLE_INCOMPLETE,
  UNSERVICEABLE_CONDEMNED
}
