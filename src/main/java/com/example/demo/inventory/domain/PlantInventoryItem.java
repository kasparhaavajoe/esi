package com.example.demo.inventory.domain;

import javax.persistence.*;

import lombok.*;

import javax.persistence.Entity;


@Entity
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryItem {

  @Id @GeneratedValue
  Long id;

  String serial_number;

  @OneToOne
  PlantInventoryEntry plantInfo;

  @Enumerated(EnumType.STRING)
  EquipmentCondition equipment_condition;
}
