package com.example.demo.inventory.domain;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Value
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class PlantInventoryEntry {

  @Id
  @GeneratedValue
  Long id;

  String name;
  String description;
  @Column(precision=8, scale=2)
  BigDecimal price;

}
