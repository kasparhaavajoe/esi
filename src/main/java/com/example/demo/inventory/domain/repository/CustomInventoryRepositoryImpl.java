package com.example.demo.inventory.domain.repository;

import com.example.demo.inventory.domain.PlantInventoryEntry;
import com.example.demo.inventory.domain.PlantInventoryItem;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.time.LocalDate;
import java.util.List;

public class CustomInventoryRepositoryImpl implements CustomInventoryRepository {

    @Autowired
    EntityManager em;

    @Override
    public List<PlantInventoryEntry> findAvailablePlants(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery(
                "select p.plantInfo from PlantInventoryItem p " +
                        "where LOWER(p.plantInfo.name) like concat('%', ?1, '%') and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.period.endDate and ?3 > r.period.startDate)",
                PlantInventoryEntry.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }

    @Override
    public List<PlantInventoryItem> findAvailableItems(String name, LocalDate startDate, LocalDate endDate) {
        return em.createQuery(
                "select p from PlantInventoryItem " +
                        "where p.plantInfo.name like ?1 and p not in " +
                        "(select r.plant from PlantReservation r where ?2 < r.period.endDate and ?3 > r.period.startDate)",
                PlantInventoryItem.class)
                .setParameter(1, name)
                .setParameter(2, startDate)
                .setParameter(3, endDate)
                .getResultList();
    }
}
