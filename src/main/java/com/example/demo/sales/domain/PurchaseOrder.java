package com.example.demo.sales.domain;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

import lombok.AccessLevel;

import com.example.demo.inventory.domain.PlantInventoryEntry;
import com.example.demo.inventory.domain.PlantReservation;
import com.example.demo.common.domain.BusinessPeriod;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Entity
@Getter
@NoArgsConstructor(force = true, access = AccessLevel.PRIVATE)
public class PurchaseOrder {

  @Id @GeneratedValue
  Long id;

  @OneToOne
  PlantInventoryEntry plant;

  @OneToMany
  List<PlantReservation> reservations;

  LocalDate issue_date;
  LocalDate payment_schedule;
  @Column(precision=8, scale=2)
  BigDecimal total;

  @Enumerated(EnumType.STRING)
  POStatus status;

  @Embedded
  BusinessPeriod rentalPeriod;

  public static PurchaseOrder of(PlantInventoryEntry entry, BusinessPeriod period) {
    PurchaseOrder po = new PurchaseOrder();
    po.plant = entry;
    po.rentalPeriod = period;
    po.reservations = new ArrayList<>();
    po.issue_date = LocalDate.now();
    po.status = POStatus.PENDING;
    return po;
  }

  public void setStatus(POStatus status) {
    this.status = status;
  }
}
