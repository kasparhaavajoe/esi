package com.example.demo.sales.application.service;

import com.example.demo.common.domain.BusinessPeriod;
import com.example.demo.inventory.domain.PlantInventoryEntry;
import com.example.demo.inventory.domain.PlantInventoryItem;
import com.example.demo.inventory.domain.PlantReservation;
import com.example.demo.inventory.domain.repository.InventoryRepository;
import com.example.demo.inventory.domain.repository.PlantInventoryEntryRepository;
import com.example.demo.inventory.domain.repository.PlantReservationRepository;
import com.example.demo.sales.application.dto.PurchaseOrderDTO;
import com.example.demo.sales.application.rest.PlantNotFoundException;
import com.example.demo.sales.domain.POStatus;
import com.example.demo.sales.domain.PurchaseOrder;
import com.example.demo.sales.domain.repository.PurchaseOrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SalesService {

    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;

    @Autowired
    PurchaseOrderAssembler purchaseOrderAssembler;

    @Autowired
    PlantReservationRepository plantReservationRepository;

    @Autowired
    InventoryRepository inventoryRepository;

    @Autowired
    PlantInventoryEntryRepository plantInventoryEntryRepository;

    public PurchaseOrderDTO findPO(Long id) throws PlantNotFoundException {
        PurchaseOrder po = purchaseOrderRepository.findById(id).orElse(null);
        if (po == null) throw new PlantNotFoundException(id);
        return purchaseOrderAssembler.toResource(po);
    }

    public PurchaseOrderDTO createPO(PurchaseOrderDTO poDTO) throws Exception {

        BusinessPeriod period = BusinessPeriod.of(
                poDTO.getRentalPeriod().getStartDate(),
                poDTO.getRentalPeriod().getEndDate()
        );
        PlantInventoryEntry plant= plantInventoryEntryRepository.findById(poDTO.getPlant().get_id()).orElse(null);

        PurchaseOrder po = PurchaseOrder.of(plant, period);
        purchaseOrderRepository.save(po);

        List<PlantInventoryItem> available = inventoryRepository.findAvailableItems(
                po.getPlant().getName(),
                po.getRentalPeriod().getStartDate(),
                po.getRentalPeriod().getEndDate()
        );

        if (available.size() == 0) {
            po.setStatus(POStatus.REJECTED);
            throw new Exception("No available items");
        }

        PlantReservation reservation = PlantReservation.of(available.get(0), po.getRentalPeriod());
        plantReservationRepository.save(reservation);

        po.getReservations().add(reservation);
        po.setStatus(POStatus.OPEN);
        purchaseOrderRepository.save(po);

        return purchaseOrderAssembler.toResource(po);
    }


}
